# Wallet App

Здесь находится пдоробная графическая структураа проекта, которая поможет быстрее разобраться в проекте
[Стурктура проекта](https://www.figma.com/file/ArPpdqP3LRmIc9dr7thLqq/Wallet-App?type=whiteboard&node-id=0-1&t=bjKEaTHiCk08Wwr0-0)

Все скрипты находятся в Assets\_Wallet\Scripts

Здесь реализовано приложение, которое позваляет модифицировать валюты через ECS согласно [ТЗ](https://docs.google.com/document/d/1xpV1SYtguWYoHjaW8v5mqqRCoNlQGz8x-0dvr79OJ7w/edit)

## Как начать работать с проектом

- Добавить на сцену GameObject и добавить к нему скрипт WalletLifetimeScope.cs
- Запустить
- (В проекте уже есть настроенная сцена Main)

## Как добавить новые валюты

- Правой клавишой мыши нажать на папку Assets/_Wallet/_Data/Configs и нажать Create/Currency/Create Currency Data 
- В созданный конфиг добавить данные - иконку и уникальное название валюты
- Добавить конфиг в файл Assets/_Wallet/_Data/Configs/All Currency Data.asset
- Новая валюта должна отобразиться на экране после запуска проекта

## Как добавить новые способы сохранения данных

- В папке Assets/_Wallet/Scripts/Services/SaveDataService находятся реализации сервисов для сохранения
- Нужно создать свой класс, реализующий интерфейс ISaveDataService
- Зарегистрировать класс в WalletLifetimeScope.cs
``builder.Register<ISaveDataService, [Новый класс]>(Lifetime.Singleton);``

## Как поменять сервис сохранения на PlayerPrefs

В WalletLifetimeScope заменить 
``builder.Register<ISaveDataService, FileService>(Lifetime.Singleton);``
на
``builder.Register<ISaveDataService, PlayerPrefsService>(Lifetime.Singleton);``

Тестовая сборка для Android https://drive.google.com/drive/folders/1K_TxkQwqDqwH2fJ5VwEGL4xTp2SlMsWF?usp=sharing