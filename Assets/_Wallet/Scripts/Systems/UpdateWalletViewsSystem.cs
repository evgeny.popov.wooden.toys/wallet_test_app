using _Wallet.Scripts.Components;
using _Wallet.Scripts.UI;
using Unity.Collections;
using Unity.Entities;
using VContainer;

namespace _Wallet.Scripts.Systems
{ 
  [DisableAutoCreation]
  [UpdateInGroup(typeof(SimulationSystemGroup))]
  [UpdateAfter(typeof(WalletSystem))]
  public partial class UpdateWalletViewsSystem : SystemBase
  {
    private IUIViewLogic _iuiViewLogic;
    private SaveWalletDataSystem _saveWalletDataSystem;

    [Inject]
    public void Construct(IUIViewLogic iuiViewLogic, SaveWalletDataSystem saveWalletDataSystem)
    {
      _saveWalletDataSystem = saveWalletDataSystem;
      _iuiViewLogic = iuiViewLogic;
    }

    public void LaunchSystem()
    {
      _saveWalletDataSystem.LaunchSystem();
      Enabled = true;
    }

    protected override void OnCreate()
    {
      base.OnCreate();
      Enabled = false;
    }
    
    protected override void OnUpdate()
    {
      CurrencyUpdateViews();

      void CurrencyUpdateViews()
      {
        var updateViewsQuery = GetEntityQuery(ComponentType.ReadOnly<CurrencyData>(), ComponentType.ReadWrite<UpdateCurrencyViewData>());

        if (updateViewsQuery.IsEmpty)
          return;
        
        foreach (var entity in updateViewsQuery.ToEntityArray(Allocator.TempJob))
        {
          _iuiViewLogic.UpdateItemView(entity.Index, EntityManager.GetComponentData<CurrencyData>(entity).count);
          EntityManager.SetComponentEnabled<UpdateCurrencyViewData>(entity, false);
        }
      }
    }
  }
}