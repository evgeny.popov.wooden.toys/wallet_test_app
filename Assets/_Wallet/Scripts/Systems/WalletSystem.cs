using System.Threading.Tasks;
using _Wallet.Scripts.Aspects;
using _Wallet.Scripts.Components;
using _Wallet.Scripts.Data;
using _Wallet.Scripts.Services.SaveDataService;
using _Wallet.Scripts.UI;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using VContainer;

namespace _Wallet.Scripts.Systems
{
  [DisableAutoCreation]
  [UpdateInGroup(typeof(SimulationSystemGroup))]
  public partial class WalletSystem : SystemBase
  {
    private IUIViewLogic _uiViewLogic;
    private ISaveDataService _saveDataService;

    private UpdateWalletViewsSystem _updateWalletViewsSystem;

    [Inject]
    public void Construct(ISaveDataService saveDataService, IUIViewLogic uiViewLogic,
      UpdateWalletViewsSystem updateWalletViewsSystem)
    {
      _saveDataService = saveDataService;
      _updateWalletViewsSystem = updateWalletViewsSystem;
      _uiViewLogic = uiViewLogic;
    }

    public async void LaunchSystem(AllCurrenciesData allCurrenciesData)
    {
      await CreateCurrencyEntities(allCurrenciesData);

      _updateWalletViewsSystem.LaunchSystem();
      Enabled = true;
    }

    protected override void OnCreate()
    {
      base.OnCreate();
      Enabled = false;
    }

    protected override void OnUpdate()
    {
      CurrencyChanges();

      void CurrencyChanges()
      {
        EntityQuery entityQuery = SystemAPI.QueryBuilder().WithAll<CurrencyData, ChangeCurrencyData>()
          .WithDisabled<SaveCurrencyData, UpdateCurrencyViewData>().Build();

        if (entityQuery.IsEmpty)
          return;
        
        var ecbSingleton = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();

        new UpdateCurrencyJob()
        {
          ecb = ecbSingleton.CreateCommandBuffer(World.Unmanaged).AsParallelWriter()
        }.ScheduleParallel(entityQuery);
      }
    }

    [BurstCompile]
    private partial struct UpdateCurrencyJob : IJobEntity
    {
      public EntityCommandBuffer.ParallelWriter ecb;

      [BurstCompile]
      public void Execute(CurrencyAspect currencyAspectData, [EntityIndexInQuery] int sortKey)
      {
        currencyAspectData.currencyData.ValueRW.count += currencyAspectData.changeCurrencyData.ValueRW.changeCount;

        if (currencyAspectData.currencyData.ValueRW.count < 0)
          currencyAspectData.currencyData.ValueRW.count = 0;

        ecb.SetComponentEnabled<ChangeCurrencyData>(sortKey, currencyAspectData.entity, false);
        
        ecb.SetComponentEnabled<UpdateCurrencyViewData>(sortKey, currencyAspectData.entity, true);
        ecb.SetComponentEnabled<SaveCurrencyData>(sortKey, currencyAspectData.entity, true);
      }
    }

    private async Task CreateCurrencyEntities(AllCurrenciesData allCurrenciesData)
    {
      SavedDataStructure savedDataStructure = _saveDataService.LoadStructure();

      await _uiViewLogic.CreateWalletScreen();

      foreach (CurrencyDataSO currencyDataSo in allCurrenciesData.Currencies)
      {
        var entity = EntityManager.CreateEntity();

        SetupCurrencyDataComponent(entity, currencyDataSo, savedDataStructure);
        SetupChangeCurrencyDataComponent(entity);
        SetupUpdateCurrencyViewDataComponent(entity);
        SetupSaveCurrencyDataComponent(entity);
        
        await CreateWalletItemView(entity, currencyDataSo);
      }

      _updateWalletViewsSystem.Enabled = true;

      void SetupCurrencyDataComponent(Entity entity, CurrencyDataSO currencyDataSo, SavedDataStructure dataStructure)
      {
        EntityManager.AddComponent<CurrencyData>(entity);

        int currencyHash = currencyDataSo.Key.GetHashCode();

        EntityManager.SetComponentData(entity, new CurrencyData
        {
          hash = currencyHash,
          count = GetCurrencyValue(dataStructure, entity.Index, currencyHash)
        });
      }

      void SetupChangeCurrencyDataComponent(Entity entity)
      {
        EntityManager.AddComponent<ChangeCurrencyData>(entity);
        EntityManager.SetComponentData(entity, new ChangeCurrencyData
        {
          changeCount = 0
        });
        EntityManager.SetComponentEnabled<ChangeCurrencyData>(entity, false);
      }
      
      
      void SetupUpdateCurrencyViewDataComponent(Entity entity)
      {
        EntityManager.AddComponent<UpdateCurrencyViewData>(entity);
        EntityManager.SetComponentEnabled<UpdateCurrencyViewData>(entity, false);
      }  
      
      void SetupSaveCurrencyDataComponent(Entity entity)
      {
        EntityManager.AddComponent<SaveCurrencyData>(entity);
        EntityManager.SetComponentEnabled<SaveCurrencyData>(entity, false);
      }
    }

    private async Task CreateWalletItemView(Entity entity, CurrencyDataSO currencyDataSo)
    {
      Actions actions = new Actions { OnIncrement = Increment, OnDecrement = Decrement, OnClear = Clear, GetCount = GetCount };

      await _uiViewLogic.CreateWalletItemView(entity.Index, currencyDataSo, actions);

      int GetCount() =>
        EntityManager.GetComponentData<CurrencyData>(entity).count;

      void Increment()
      {
        EntityManager.SetComponentData(entity, new ChangeCurrencyData { changeCount = 1 });
        EntityManager.SetComponentEnabled<ChangeCurrencyData>(entity, true);
      }

      void Decrement()
      {
        EntityManager.SetComponentData(entity, new ChangeCurrencyData { changeCount = -1 });
        EntityManager.SetComponentEnabled<ChangeCurrencyData>(entity, true);
      }

      void Clear()
      {
        EntityManager.SetComponentData(entity, new ChangeCurrencyData { changeCount = -EntityManager.GetComponentData<CurrencyData>(entity).count });
        EntityManager.SetComponentEnabled<ChangeCurrencyData>(entity, true);
      }
    }

    private int GetCurrencyValue(SavedDataStructure dataStructure, int entityIndex, int currencyHash)
    {
      if (dataStructure.currencyValues.ContainsKey(entityIndex))
        if (dataStructure.currencyValues[entityIndex].ContainsKey(currencyHash))
          return dataStructure.currencyValues[entityIndex][currencyHash];

      return 0;
    }
  }
}