using System.Collections.Generic;
using System.Threading.Tasks;
using _Wallet.Scripts.Components;
using _Wallet.Scripts.Services.SaveDataService;
using _Wallet.Scripts.UI;
using Unity.Collections;
using Unity.Entities;
using VContainer;

namespace _Wallet.Scripts.Systems
{
  [DisableAutoCreation]
  [UpdateInGroup(typeof(SimulationSystemGroup))]
  [UpdateAfter(typeof(WalletSystem))]
  public partial class SaveWalletDataSystem : SystemBase
  {
    private IUIViewLogic _uiViewLogic;
    private ISaveDataService _saveDataService;

    private SavedDataStructure _currentDataStructure;

    [Inject]
    public void Construct(IUIViewLogic uiViewLogic, ISaveDataService saveDataService)
    {
      _saveDataService = saveDataService;
      _uiViewLogic = uiViewLogic;
    }

    public async void LaunchSystem()
    {
      _currentDataStructure = _saveDataService.LoadStructure();
      await _uiViewLogic.CreateSaveView(OnSave);

      Enabled = true;
      
      async Task OnSave() => 
        await _saveDataService.SaveStructure(_currentDataStructure);
    }

    protected override void OnCreate()
    {
      base.OnCreate();
      Enabled = false;
    }

    protected override void OnUpdate()
    {
      CurrencyReadData();

      void CurrencyReadData()
      {
        var updateViewsQuery = GetEntityQuery(ComponentType.ReadOnly<CurrencyData>(), ComponentType.ReadWrite<SaveCurrencyData>());

        if (updateViewsQuery.IsEmpty)
          return;

        foreach (var entity in updateViewsQuery.ToEntityArray(Allocator.TempJob))
        {
          OnValuesRead(entity.Index, EntityManager.GetComponentData<CurrencyData>(entity));
          EntityManager.SetComponentEnabled<SaveCurrencyData>(entity, false);
        }
      }
    }

    private void OnValuesRead(int entityIndex, CurrencyData currencyData)
    {
      if (_currentDataStructure.currencyValues.ContainsKey(entityIndex))
      {
        if (_currentDataStructure.currencyValues[entityIndex].ContainsKey(currencyData.hash))
          _currentDataStructure.currencyValues[entityIndex][currencyData.hash] = currencyData.count;
        else
          _currentDataStructure.currencyValues[entityIndex].Add(currencyData.hash, currencyData.count);
      }
      else
        _currentDataStructure.currencyValues.Add(entityIndex, new Dictionary<int, int>());
    }
  }
}