using _Wallet.Scripts.Services.AssetProvider;
using _Wallet.Scripts.Services.CurrencyDataService;
using _Wallet.Scripts.Services.SaveDataService;
using _Wallet.Scripts.Systems;
using _Wallet.Scripts.UI;
using _Wallet.Scripts.UI.Interfaces;
using VContainer;
using VContainer.Unity;

namespace _Wallet.Scripts.VContainer
{
  public class WalletLifetimeScope : LifetimeScope
  {
    protected override void Configure(IContainerBuilder builder)
    {
      builder.RegisterEntryPoint<GameBootstrapper>();
      
      builder.RegisterSystemIntoDefaultWorld<WalletSystem>();
      builder.RegisterSystemIntoDefaultWorld<UpdateWalletViewsSystem>();
      builder.RegisterSystemIntoDefaultWorld<SaveWalletDataSystem>();

      builder.Register<IAssetProvider, AssetProvider>(Lifetime.Singleton);
      builder.Register<IUIFactory, UIFactory>(Lifetime.Singleton);
      builder.Register<IUIViewLogic, UIViewLogic>(Lifetime.Singleton);

      builder.Register<ICurrencyDataService, CurrencyDataService>(Lifetime.Singleton);
      builder.Register<ISaveDataService, FileService>(Lifetime.Singleton);
    }
  }
}