using System;
using _Wallet.Scripts.Data;
using _Wallet.Scripts.Services.CurrencyDataService;
using _Wallet.Scripts.Systems;
using _Wallet.Scripts.UI.Interfaces;
using VContainer.Unity;

namespace _Wallet.Scripts.VContainer
{
  public class GameBootstrapper : IStartable, IDisposable
  {
    private readonly ICurrencyDataService _currencyDataService;
    private readonly IUIFactory _uiFactory;
    
    private readonly WalletSystem _walletSystem;

    public GameBootstrapper(IUIFactory uiFactory, ICurrencyDataService currencyDataService, WalletSystem walletSystem)
    {
      _uiFactory = uiFactory;
      _walletSystem = walletSystem;
      _currencyDataService = currencyDataService;
    }
    
    public void Start() => 
      _currencyDataService.LoadConfigs(OnComplete);

    private void OnComplete(AllCurrenciesData allCurrenciesData) => 
      _walletSystem.LaunchSystem(allCurrenciesData);

    public void Dispose()
    {
      _currencyDataService.Release();
      _uiFactory.Release();
    }
  }
}