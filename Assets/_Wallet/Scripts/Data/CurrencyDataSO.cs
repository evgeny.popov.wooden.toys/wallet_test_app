using UnityEngine;

namespace _Wallet.Scripts.Data
{
  [CreateAssetMenu(fileName = "Currency Data", menuName = "Currency/Create Currency Data", order = 0)]
  // ReSharper disable once InconsistentNaming
  public class CurrencyDataSO : ScriptableObject
  {
    public string Key;
    public Sprite Icon;
  }
}