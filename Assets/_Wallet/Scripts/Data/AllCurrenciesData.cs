using UnityEngine;

namespace _Wallet.Scripts.Data
{
  [CreateAssetMenu(fileName = "All Currency Data", menuName = "Currency/Create All Currency Data", order = 0)]
  public class AllCurrenciesData : ScriptableObject
  {
    public CurrencyDataSO[] Currencies;
  }
}