using Unity.Entities;

namespace _Wallet.Scripts.Components
{
  public struct ChangeCurrencyData : IComponentData, IEnableableComponent
  {
    public int changeCount;
  }
}