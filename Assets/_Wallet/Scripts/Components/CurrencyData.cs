using Unity.Entities;

namespace _Wallet.Scripts.Components
{
  public struct CurrencyData : IComponentData
  {
    public int hash;
    public int count;
  }
}