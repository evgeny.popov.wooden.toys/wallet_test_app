using _Wallet.Scripts.Components;
using Unity.Entities;

namespace _Wallet.Scripts.Aspects
{
  public readonly partial struct CurrencyAspect : IAspect
  {
    public readonly Entity entity;
    
    public readonly RefRW<CurrencyData> currencyData;
    public readonly RefRW<ChangeCurrencyData> changeCurrencyData;
    
    public readonly RefRW<UpdateCurrencyViewData> updateCurrencyViewData;
    public readonly RefRW<SaveCurrencyData> saveCurrencyData;
  }
}