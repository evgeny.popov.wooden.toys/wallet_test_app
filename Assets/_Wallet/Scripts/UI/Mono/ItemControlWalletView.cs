using System;
using _Wallet.Scripts.Data;
using _Wallet.Scripts.UI.Interfaces;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Wallet.Scripts.UI.Mono
{
  public class ItemControlWalletView : MonoBehaviour, IItemControlWalletView
  {
    [Header("Views")]
    public TMP_Text CounterText;
    public Image IconImage;

    [Header("Buttons")]
    
    public Button IncrementButton;
    public Button DecrementButton;
    public Button ClearButton;
    
    private int _count =-1;

    public void SetupView(Sprite previewIcon, Action onIncrement, Action onDecrement, Action onClear)
    {
      IconImage.sprite = previewIcon;

      IncrementButton.onClick.AddListener(onIncrement.Invoke);
      DecrementButton.onClick.AddListener(onDecrement.Invoke);
      
      ClearButton.onClick.AddListener(onClear.Invoke);
    }

    public void SetCount(int count)
    {
      if(_count == count)
        return;
      
      _count = count;
      CounterText.text = count.ToString();
    }
  }
}