using System;
using _Wallet.Scripts.UI.Interfaces;
using UnityEngine;
using UnityEngine.UI;

namespace _Wallet.Scripts.UI.Mono
{
  public class WalletSaveView: MonoBehaviour, IWalletSaveView
  {
    public Button SaveButton;
    public GameObject LoadingCurtain;

    public void SetupView(Action onClick) => 
      SaveButton.onClick.AddListener(onClick.Invoke);

    public void ShowLoading(bool isActive) => 
      LoadingCurtain.SetActive(isActive);
  }
}