using _Wallet.Scripts.UI.Interfaces;
using UnityEngine;

namespace _Wallet.Scripts.UI.Mono
{
  public class WalletScreenView : MonoBehaviour, IWalletScreenView
  {
    public Transform Content;
    public Transform ButtonParent;

    public Transform GetContent() => 
      Content;   
    
    public Transform GetButtonParent() => 
      ButtonParent;
  }
}