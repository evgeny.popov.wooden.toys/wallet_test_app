using System;
using System.Threading.Tasks;
using _Wallet.Scripts.Data;

namespace _Wallet.Scripts.UI
{
  public interface IUIViewLogic
  {
    Task CreateWalletScreen();
    Task CreateWalletItemView(int entityId, CurrencyDataSO currencyDataSo, Actions onClickActions);
    void UpdateItemView(int entityId, int count);
    Task CreateSaveView(OnSave onSaveClick);
  }
}