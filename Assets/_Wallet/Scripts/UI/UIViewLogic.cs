using System;
using System.Threading.Tasks;
using _Wallet.Scripts.Data;
using _Wallet.Scripts.UI.Interfaces;

namespace _Wallet.Scripts.UI
{
  public class UIViewLogic : IUIViewLogic
  {
    private readonly IUIFactory _uiFactory;
    
    private IWalletScreenView _walletScreenView;

    public UIViewLogic(IUIFactory uiFactory) => 
      _uiFactory = uiFactory;

    public async Task CreateWalletScreen() => 
      _walletScreenView = await _uiFactory.CreateWalletScreenView();

    public void UpdateItemView(int entityId, int count) => 
      _uiFactory.GetWalletItem(entityId).SetCount(count);

    public async Task CreateWalletItemView(int entityId, CurrencyDataSO currencyDataSo, Actions onClickActions)
    {
      if (_walletScreenView == null)
        throw new Exception("Parent screen not spawned yet");
      
      _walletScreenView ??= await _uiFactory.CreateWalletScreenView();

      IItemControlWalletView itemControlWalletView = await _uiFactory.CreateWalletItemView(entityId, _walletScreenView.GetContent());

      itemControlWalletView.SetupView(currencyDataSo.Icon, onClickActions.OnIncrement, onClickActions.OnDecrement, onClickActions.OnClear);
      itemControlWalletView.SetCount(onClickActions.GetCount());
    }

    public async Task CreateSaveView(OnSave onSaveClick)
    {     
      if (_walletScreenView == null)
        throw new Exception("Parent screen not spawned yet");
      
      IWalletSaveView walletSaveView = await _uiFactory.CreateWalletSaveView(_walletScreenView.GetButtonParent());
      walletSaveView.SetupView(Save);

      async void Save()
      {
        walletSaveView.ShowLoading(true);
        await onSaveClick.Invoke();
        walletSaveView.ShowLoading(false);
      }
    }
  }

  public struct Actions
  {
    public Action OnIncrement;
    public Action OnDecrement;
    public Action OnClear;
    public GetCount GetCount;
  }

  public delegate int GetCount();
  public delegate Task OnSave();
}