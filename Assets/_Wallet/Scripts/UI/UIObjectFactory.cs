using System.Threading.Tasks;
using _Wallet.Scripts.Services.AssetProvider;
using UnityEngine;

namespace _Wallet.Scripts.UI
{
  public abstract class UIObjectFactory
  {
    protected readonly IAssetProvider assetProvider;
    private Transform _parent;

    protected UIObjectFactory(IAssetProvider assetProvider) => 
      this.assetProvider = assetProvider;

    protected async Task<T> CreateUIObject<T>(string address, bool withoutParent = false)
    {
      var gameObject = await assetProvider.Load<GameObject>(address);

      T obj = withoutParent ?
        Object.Instantiate(gameObject).GetComponent<T>() :
        Object.Instantiate(gameObject, _parent).GetComponent<T>();
      
      return obj;
    }
    
    protected async Task<T> CreateUIObject<T>(string address, Transform parent)
    {
      var gameObject = await assetProvider.Load<GameObject>(address);
      T obj = Object.Instantiate(gameObject, parent).GetComponent<T>();
      
      return obj;
    }
    
    protected void Clean(Component component, string address)
    {
      Clean(component);
      assetProvider.Release(address);
    }

    protected void Clean(Component component)
    {
      if(component != null)
        Object.Destroy(component.gameObject);
    }
  }
}