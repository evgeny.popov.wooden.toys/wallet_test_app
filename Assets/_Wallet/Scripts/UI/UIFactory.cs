using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using _Wallet.Scripts.Services.AssetProvider;
using _Wallet.Scripts.UI.Interfaces;
using _Wallet.Scripts.UI.Mono;
using UnityEngine;

namespace _Wallet.Scripts.UI
{
  public class UIFactory : UIObjectFactory, IUIFactory
  {
    private const string WalletScreenAddress = "Wallet Canvas";
    private const string WalletItemAddress = "Player UI";
    private const string WalletSaveAddress = "Save View";

    private WalletScreenView _screen;
    private WalletSaveView _saveButton;
    private Dictionary<int, ItemControlWalletView> _items = new();

    public UIFactory(IAssetProvider assetProvider) : base(assetProvider)
    { }

    public async Task<IItemControlWalletView> CreateWalletItemView(int entityIndex, Transform content)
    {
      ItemControlWalletView item = await CreateUIObject<ItemControlWalletView>(WalletItemAddress, content);
      _items.Add(entityIndex, item);
      
      return item;
    }

    public IItemControlWalletView GetWalletItem(int entityIndex) =>
      _items[entityIndex];

    public async Task<IWalletScreenView> CreateWalletScreenView()
    {
      if (_screen == null)
        _screen = await CreateUIObject<WalletScreenView>(WalletScreenAddress);
      return _screen;
    }

    public async Task<IWalletSaveView> CreateWalletSaveView(Transform parent)
    {
      _saveButton = await CreateUIObject<WalletSaveView>(WalletSaveAddress, parent);
      return _saveButton;
    }

    public void Release()
    {
      foreach (var itemControlWalletView in _items)
        Clean(itemControlWalletView.Value);

      _items.Clear();
      assetProvider.Release(WalletItemAddress);
      
      Clean(_screen, WalletScreenAddress);
      Clean(_saveButton, WalletSaveAddress);

      _items = null;
      _screen = null;
      _saveButton = null;
    }
  }
}