using System;
using UnityEngine;

namespace _Wallet.Scripts.UI.Interfaces
{
  public interface IItemControlWalletView
  {
    public void SetupView(Sprite previewIcon, Action onIncrement, Action onDecrement, Action onClear);
    void SetCount(int count);
  }
}