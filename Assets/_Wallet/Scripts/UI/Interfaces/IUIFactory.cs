using System.Threading.Tasks;
using UnityEngine;

namespace _Wallet.Scripts.UI.Interfaces
{
  public interface IUIFactory
  {  
    public Task<IItemControlWalletView> CreateWalletItemView(int entityIndex, Transform getContent);
    public Task<IWalletScreenView> CreateWalletScreenView();
    public Task<IWalletSaveView> CreateWalletSaveView(Transform parent);
    public IItemControlWalletView GetWalletItem(int entityIndex);
    public void Release();
  }
}