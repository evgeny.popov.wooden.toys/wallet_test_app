using UnityEngine;

namespace _Wallet.Scripts.UI.Interfaces
{
  public interface IWalletScreenView
  {
    Transform GetContent();
    Transform GetButtonParent();
  }
}