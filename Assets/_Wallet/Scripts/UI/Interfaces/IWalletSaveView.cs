using System;

namespace _Wallet.Scripts.UI.Interfaces
{
  public interface IWalletSaveView
  {
    void SetupView(Action onClick);
    void ShowLoading(bool isActive);
  }
}