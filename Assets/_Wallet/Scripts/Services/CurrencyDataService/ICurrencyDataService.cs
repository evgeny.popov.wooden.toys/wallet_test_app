using System;
using _Wallet.Scripts.Data;

namespace _Wallet.Scripts.Services.CurrencyDataService
{
  public interface ICurrencyDataService
  {
    void LoadConfigs(Action<AllCurrenciesData> onComplete);
    void Release();
  }
}