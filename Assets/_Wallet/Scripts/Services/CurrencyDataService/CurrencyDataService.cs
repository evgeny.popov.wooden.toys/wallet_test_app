using System;
using _Wallet.Scripts.Data;
using _Wallet.Scripts.Services.AssetProvider;

namespace _Wallet.Scripts.Services.CurrencyDataService
{
  public class CurrencyDataService : ICurrencyDataService
  {
    private const string AllCurrenciesDataAddress = "All Currency Data";
    
    private readonly IAssetProvider _assetProvider;

    public CurrencyDataService(IAssetProvider assetProvider) => 
      _assetProvider = assetProvider;

    public async void LoadConfigs(Action<AllCurrenciesData> onComplete)
    {
      var allCurrenciesData = await _assetProvider.Load<AllCurrenciesData>(AllCurrenciesDataAddress);
      onComplete?.Invoke(allCurrenciesData);
    }

    public void Release() => 
      _assetProvider.Release(AllCurrenciesDataAddress);
  }
}