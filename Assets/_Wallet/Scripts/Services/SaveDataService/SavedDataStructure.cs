using System;
using System.Collections.Generic;

namespace _Wallet.Scripts.Services.SaveDataService
{
  [Serializable]
  public struct SavedDataStructure
  {
    public Dictionary<int, Dictionary<int, int>> currencyValues;
  }
}