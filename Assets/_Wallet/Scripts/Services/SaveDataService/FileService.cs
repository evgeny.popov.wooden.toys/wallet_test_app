using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using UnityEngine;

namespace _Wallet.Scripts.Services.SaveDataService
{
  public class FileService : ISaveDataService
  {   
    private const string FileName = "CurrencyValues.json";
    
    private string FilePath => Path.Combine(Application.persistentDataPath, FileName);

    public async Task SaveStructure(SavedDataStructure savedDataStructure)
    {
      string json = JsonConvert.SerializeObject(savedDataStructure);
      await WriteTextAsync(json);
    }

    public SavedDataStructure LoadStructure()
    {
      string json = ReadText();

      var saveData = string.IsNullOrEmpty(json) ?
        new SavedDataStructure { currencyValues = new Dictionary<int, Dictionary<int, int>>() } 
        : JsonConvert.DeserializeObject<SavedDataStructure>(json);
      
      return saveData;
    }

    private async Task WriteTextAsync(string text)
    {
      Debug.Log($"Write to {FilePath}");
      
      byte[] encodedText = Encoding.UTF8.GetBytes(text);

      await using FileStream sourceStream = new FileStream(FilePath,
        FileMode.Create, FileAccess.Write, FileShare.None,
        bufferSize: 4096, useAsync: true);
      
      await sourceStream.WriteAsync(encodedText, 0, encodedText.Length);
    }

    private string ReadText()
    {
      if (!File.Exists(FilePath))
      {
        string createText = "";
        File.WriteAllText(FilePath, createText);
      }
      
      Debug.Log($"Read from {FilePath}");

      return File.ReadAllText(FilePath);
    }
  }
}