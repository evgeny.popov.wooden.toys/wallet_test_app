using System.Collections.Generic;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Newtonsoft.Json;
using UnityEngine;

namespace _Wallet.Scripts.Services.SaveDataService
{
  [UsedImplicitly]
  public class PlayerPrefsService : ISaveDataService
  {
    private const string JsonKey = "PlayerPrefsService.CurrencyValues";
    
    public async Task SaveStructure(SavedDataStructure savedDataStructure)
    {
      string output = JsonConvert.SerializeObject(savedDataStructure);
      
      PlayerPrefs.SetString(JsonKey, output);
      await Task.CompletedTask;
    }

    public SavedDataStructure LoadStructure()
    {
      string json = PlayerPrefs.GetString(JsonKey);

      var saveData = string.IsNullOrEmpty(json) ?
        new SavedDataStructure { currencyValues = new Dictionary<int, Dictionary<int, int>>() } 
        : JsonConvert.DeserializeObject<SavedDataStructure>(json);
      
      return saveData;
    }
  }
}