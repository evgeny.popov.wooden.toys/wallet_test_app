using System.Threading.Tasks;

namespace _Wallet.Scripts.Services.SaveDataService
{
  public interface ISaveDataService
  {
    public Task SaveStructure(SavedDataStructure savedDataStructure);
    public SavedDataStructure LoadStructure();
  }
}