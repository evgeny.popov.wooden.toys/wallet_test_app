using System.Threading.Tasks;

namespace _Wallet.Scripts.Services.AssetProvider
{
  public interface IAssetProvider
  {
    Task<T> Load<T>(string address) where T : class;
    void Release(string address);
  }
}